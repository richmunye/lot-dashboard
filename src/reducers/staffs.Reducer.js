import { GET_COMPANY } from '../actionTypes/staffs.types';
const initialState = {
  companies: [],
  error: {},
};

const getCompanies = (state = initialState, action) => {
  switch (action.type) {
    case GET_COMPANY:
      console.log('getting all companies');
      return { ...state, companies: action.payload.data };
    default:
      return state;
  }
};
export default getCompanies;
