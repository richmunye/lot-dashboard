import { combineReducers } from 'redux';
import getCompanies from './staffs.Reducer';

const RootReducer = combineReducers({
  dashboard: getCompanies,
});
export default RootReducer;
