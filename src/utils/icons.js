import home_icon from '../assets/Icon ionic-md-home.svg';
import appointments_icon from '../assets/Icon ionic-ios-calendar.svg';
import staffs_icon from '../assets/Icon awesome-user-alt.svg';
import reports_icon from '../assets/article_black_24dp.svg';
import settings_icon from '../assets/Icon material-settings.svg';
import hide_icon from '../assets/Hide.svg';

export const icons = {
  home: home_icon,
  appointments: appointments_icon,
  staffs: staffs_icon,
  reports: reports_icon,
  settings: settings_icon,
  hide: hide_icon,
};
