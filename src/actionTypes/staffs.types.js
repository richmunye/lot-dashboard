export const GET_COMPANY = 'GET_COMPANY';
export const FAILED_GET_COMPANY = 'FAILED_GET_COMPANY';
export const GET_COMPANY_LOADING = 'GET_COMPANY_LOADING';
export const GET_EMPLOYEES = 'GET_EMPLOYEES';
export const GET_EMPLOYEES_LOADING =
  'GET_EMPLOYEES_LOADING';
