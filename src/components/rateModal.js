import React from 'react';
import { Modal } from 'antd';
import RateForm from './form/rateForm';
const RateModal = ({
  visibleModal,
  handleCancel,
  handleOk,
}) => {
  return (
    <div>
      <RateForm handleOk={handleOk} />
    </div>
  );
};
export default RateModal;
