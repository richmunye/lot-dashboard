import { Table, Space, Typography } from 'antd';
import { Image } from 'antd';
import Ellipse1 from '../assets/ellipse-1.png';
import Ellipse2 from '../assets/ellipse-2.png';
import Ellipse3 from '../assets/ellipse-3.png';
import Ellipse4 from '../assets/ellipse-4.png';
import { MdEdit, MdDelete } from 'react-icons/md';
import './table.css';

const { Text } = Typography;

const columns = [
  {
    // title: 'COMPANIES',
    dataIndex: 'employee',
    key: 'employee',
  },
  {
    key: 'company',
    dataIndex: 'company',
  },
  {
    // title: 'SERVICE FEE',
    key: 'editDelete',
    dataIndex: 'editDelete',
    // render:
  },
];

const dataSource = [
  {
    key: 6,
    employee: (
      <Space direction="horizontal">
        <Image
          className="table-pic-img"
          width={30}
          height={30}
          src={Ellipse1}
          alt="mtn-logo"
        />
        <Text className="company">Kamikazi Lilian</Text>
      </Space>
    ),
    company: (
      <Text className="date-joined">Equity Bank</Text>
    ),
    editDelete: (
      <div className="rate-button-table">
        <MdEdit />
        <MdDelete />
      </div>
    ),
  },
  {
    key: 2,
    employee: (
      <Space direction="horizontal">
        <Image
          className="table-pic-img"
          width={30}
          height={30}
          src={Ellipse2}
          alt="bk-bank-logo"
        />
        <Text className="company">Kamikazi Lilian</Text>
      </Space>
    ),
    company: <Text className="date-joined">KCB Bank</Text>,
    editDelete: (
      <div className="rate-button-table">
        <MdEdit />
        <MdDelete />
      </div>
    ),
  },
  {
    key: 3,
    employee: (
      <Space direction="horizontal">
        <Image
          className="table-pic-img"
          width={30}
          height={30}
          src={Ellipse1}
          alt="bralirwa-logo"
        />
        <Text className="company">Kamikazi Lilian</Text>
      </Space>
    ),
    company: (
      <Text className="date-joined">Bank of Kigali</Text>
    ),
    editDelete: (
      <div className="rate-button-table">
        <MdEdit />
        <MdDelete />
      </div>
    ),
  },
  {
    key: 4,
    employee: (
      <Space direction="horizontal">
        <Image
          className="table-pic-img"
          width={30}
          height={30}
          src={Ellipse3}
          alt="kcb-bank-logo"
        />
        <Text className="company">Kamikazi Lilian</Text>
      </Space>
    ),
    company: <Text className="date-joined">Airtel</Text>,
    editDelete: (
      <div className="rate-button-table">
        <MdEdit />
        <MdDelete />
      </div>
    ),
  },
  {
    key: 5,
    employee: (
      <Space direction="horizontal">
        <Image
          className="table-pic-img"
          width={30}
          height={30}
          src={Ellipse4}
          alt="equity-bank-logo"
        />
        <Text className="company">Kamikazi Lilian</Text>
      </Space>
    ),
    company: <Text className="date-joined">MTN</Text>,
    editDelete: (
      <div className="rate-button-table">
        <MdEdit />
        <MdDelete />
      </div>
    ),
  },
];

const Employees = () => {
  return (
    <Table
      className="students-table"
      columns={columns}
      dataSource={dataSource}
    />
  );
};

export default Employees;
