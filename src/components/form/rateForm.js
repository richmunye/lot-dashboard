import { Col, Typography, Button, Rate } from 'antd';
import MtnLogo from '../../assets/mtn-circle.png';

const { Text, Title } = Typography;

const RateForm = ({ handleOk }) => (
  <div style={{ marginBottom: '0px' }}>
    <Col>
      <Title
        level={4}
        style={{
          marginBottom: '20px',
          color: 'blueviolet',
        }}
      >
        Rate a Company
      </Title>
    </Col>
    <Col style={{ padding: '32px' }}>
      <img src={MtnLogo} alt="company-logo" />
    </Col>
    <Col>
      <Title style={{ fontSize: '23px' }}>MTN Rwanda</Title>
    </Col>
    <Col>
      <Text style={{ opacity: '.65' }}>
        To rate this company you will first choose the stars
        and save by ckicking rate
      </Text>
    </Col>
    <Col style={{ padding: '32px' }}>
      <Text style={{ padding: '12px' }}>Rate</Text>
      <Rate allowHalf style={{ marginBottom: '23px' }} />
      <Button onClick={handleOk} style={{ color: 'white' }}>
        Rate
      </Button>
    </Col>
  </div>
);
export default RateForm;
