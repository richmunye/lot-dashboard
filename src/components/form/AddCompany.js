import {
  Form,
  Input,
  Button,
  Select,
  Row,
  Col,
  Typography,
} from 'antd';

const { Option } = Select;
const { Title } = Typography;

const AddForm = () => (
  <Row>
    <Title>Add a Company</Title>
    <Row>
      <Col>{/* <UserOutlined/> */}</Col>
    </Row>
    <Row>
      <Form>
        <Form.Item
          name="company Name"
          rules={[
            {
              required: true,
              message: 'company name is required',
            },
          ]}
        >
          <Input placeholder="Name of the company" />
        </Form.Item>
        <Form.Item>
          <Select
            placeholder="size of the company"
            optionFilterProp="children"
          >
            <Option value="small">
              Small Company(10-100)
            </Option>
            <Option value="medium">
              Medium Company(10-100)
            </Option>
          </Select>
        </Form.Item>
        <Form.Item>
          <Button>Add</Button>
        </Form.Item>
      </Form>
    </Row>
  </Row>
);
export default AddForm;
