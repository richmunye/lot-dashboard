import React, { useState } from 'react';
import { Layout, Menu, Col, Row } from 'antd';
import AppointmentSvg from '../../assets/Icon ionic-ios-calendar.svg';
import StaffIcon from '../../assets/Icon awesome-user-alt.svg';
import ReportsIcon from '../../assets/article_black_24dp.svg';
import SettingsIcon from '../../assets/Icon material-settings.svg';
import ProfilePic from '../../assets/profile-pic.png';
import HideIconOn from '../../assets/hide.png';
import { useHistory } from 'react-router-dom';

const { Sider } = Layout;

const Sidebar = () => {
  const [collapsed, setcollapsed] = useState(false);
  const history = useHistory();

  const [darkmode] = useState('light');
  const [active, setActive] = useState('dashboard');
  const onCollapse = (collapsed) => {
    setcollapsed(onCollapse);
  };

  const collapsedChange = () => {
    setcollapsed(!collapsed);
  };

  return (
    <Sider
      theme={'darkmode'}
      className={`dashboard-sidebar-routes ${
        darkmode === 'dark' ? 'dark_mode_background' : ''
      }`}
      trigger={null}
      collapsible
      width={250}
      collapsed={collapsed}
      onCollapse={onCollapse}
    >
      {/* profile pic  */}

      <div
        className={`profile-pic ${
          darkmode === 'dark' ? 'dark_mode_background' : ''
        }`}
      >
        {collapsed ? (
          <Row>
            <Col>
              {' '}
              <img src={ProfilePic} alt="profile" />
            </Col>
          </Row>
        ) : (
          <Row>
            <Col>
              {' '}
              <img src={ProfilePic} alt="profile" />
            </Col>
            <Col>
              <p className="username">Dr Simmons M.</p>
              <p className="role">Admin</p>
            </Col>
          </Row>
        )}
      </div>

      <Menu
        theme={darkmode}
        defaultSelectedKeys={['1']}
        mode="inline"
      >
        <Menu.Item
          onClick={() => history.push('/dashboard')}
          className="light_green_color dashboard-links"
          key="1"
          icon={
            <svg
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 24 24"
              width="24"
              height="24"
            >
              <path fill="none" d="M0 0h24v24H0z" />
              <path
                d="M20 20a1 1 0 0 1-1 1H5a1 1 0 0 1-1-1v-9H1l10.327-9.388a1 1 0 0 1 1.346 0L23 11h-3v9zm-9-7v6h2v-6h-2z"
                fill="rgba(255,255,255,1)"
              />
            </svg>
          }
        >
          Home
        </Menu.Item>

        <Menu.Item
          key="2"
          onClick={() => history.push('/appointments')}
          className="dashboard-links dark_blue_color"
          to="/appointments"
          icon={
            darkmode === 'dark' ? (
              <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 24 24"
                width="24"
                height="24"
              >
                <path fill="none" d="M0 0h24v24H0z" />
                <path
                  d="M2 11h20v9a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1v-9zm15-8h4a1 1 0 0 1 1 1v5H2V4a1 1 0 0 1 1-1h4V1h2v2h6V1h2v2z"
                  fill="rgba(255,255,255,1)"
                />
              </svg>
            ) : (
              <img
                class="dashboard-navbar-icon "
                src={AppointmentSvg}
                alt="appointments"
              />
            )
          }
        >
          Appointments
        </Menu.Item>

        <Menu.Item
          key="3"
          onClick={() => history.push('/staffs')}
          className="dashboard-links dark_blue_color font-weight-medium"
          icon={
            darkmode === 'dark' ? (
              <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 24 24"
                width="24"
                height="24"
              >
                <path fill="none" d="M0 0h24v24H0z" />
                <path
                  d="M20 22H4v-2a5 5 0 0 1 5-5h6a5 5 0 0 1 5 5v2zm-8-9a6 6 0 1 1 0-12 6 6 0 0 1 0 12z"
                  fill="rgba(255,255,255,1)"
                />
              </svg>
            ) : (
              <img
                class="dashboard-navbar-icon "
                src={StaffIcon}
                alt="staff-icon"
              />
            )
          }
        >
          Staff
        </Menu.Item>

        <Menu.Item
          key="4"
          onClick={() => history.push('/reports')}
          className="dashboard-links dark_blue_color font-weight-medium"
          icon={
            darkmode === 'dark' ? (
              <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 24 24"
                width="24"
                height="24"
              >
                <path fill="none" d="M0 0h24v24H0z" />
                <path
                  d="M20 22H4a1 1 0 0 1-1-1V3a1 1 0 0 1 1-1h16a1 1 0 0 1 1 1v18a1 1 0 0 1-1 1zM8 7v2h8V7H8zm0 4v2h8v-2H8zm0 4v2h5v-2H8z"
                  fill="rgba(255,252,252,1)"
                />
              </svg>
            ) : (
              <img
                class="dashboard-navbar-icon "
                src={ReportsIcon}
                alt="report-svg"
              />
            )
          }
        >
          Reports
        </Menu.Item>

        <Menu.Item
          key="5"
          onClick={() => setActive('settings')}
          className="dashboard-links dark_blue_color font-weight-medium"
          icon={
            darkmode === 'dark' ? (
              <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 24 24"
                width="24"
                height="24"
              >
                <path fill="none" d="M0 0h24v24H0z" />
                <path
                  d="M2.132 13.63a9.942 9.942 0 0 1 0-3.26c1.102.026 2.092-.502 2.477-1.431.385-.93.058-2.004-.74-2.763a9.942 9.942 0 0 1 2.306-2.307c.76.798 1.834 1.125 2.764.74.93-.385 1.457-1.376 1.43-2.477a9.942 9.942 0 0 1 3.262 0c-.027 1.102.501 2.092 1.43 2.477.93.385 2.004.058 2.763-.74a9.942 9.942 0 0 1 2.307 2.306c-.798.76-1.125 1.834-.74 2.764.385.93 1.376 1.457 2.477 1.43a9.942 9.942 0 0 1 0 3.262c-1.102-.027-2.092.501-2.477 1.43-.385.93-.058 2.004.74 2.763a9.942 9.942 0 0 1-2.306 2.307c-.76-.798-1.834-1.125-2.764-.74-.93.385-1.457 1.376-1.43 2.477a9.942 9.942 0 0 1-3.262 0c.027-1.102-.501-2.092-1.43-2.477-.93-.385-2.004-.058-2.763.74a9.942 9.942 0 0 1-2.307-2.306c.798-.76 1.125-1.834.74-2.764-.385-.93-1.376-1.457-2.477-1.43zM12 15a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"
                  fill="rgba(255,255,255,1)"
                />
              </svg>
            ) : (
              <img
                class="dashboard-navbar-icon "
                src={SettingsIcon}
                alt="settings"
              />
            )
          }
        >
          Settings
        </Menu.Item>

        {collapsed && darkmode === 'dark' ? (
          <svg
            onClick={collapsedChange}
            className="hide-icon-on"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 24 24"
            width="24"
            height="24"
          >
            <path fill="none" d="M0 0h24v24H0z" />
            <path
              d="M13.172 12l-4.95-4.95 1.414-1.414L16 12l-6.364 6.364-1.414-1.414z"
              fill="rgba(255,255,255,1)"
            />
          </svg>
        ) : collapsed && darkmode === 'light' ? (
          <svg
            onClick={collapsedChange}
            className="hide-icon-on"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 24 24"
            width="24"
            height="24"
          >
            <path fill="none" d="M0 0h24v24H0z" />
            <path
              d="M13.172 12l-4.95-4.95 1.414-1.414L16 12l-6.364 6.364-1.414-1.414z"
              fill="rgba(27,39,175,1)"
            />
          </svg>
        ) : !collapsed && darkmode === 'dark' ? (
          <Row
            onClick={collapsedChange}
            className="hide-icon-on"
          >
            <Col>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 24 24"
                width="24"
                height="24"
              >
                <path fill="none" d="M0 0h24v24H0z" />
                <path
                  d="M10.828 12l4.95 4.95-1.414 1.414L8 12l6.364-6.364 1.414 1.414z"
                  fill="rgba(255,255,255,1)"
                />
              </svg>
            </Col>
            <Col>
              <p style={{ marginTop: '0.9em' }}>Hide</p>
            </Col>
          </Row>
        ) : (
          <img
            onClick={collapsedChange}
            className="hide-icon-on"
            src={HideIconOn}
            alt="hide-icon"
          />
        )}
      </Menu>
    </Sider>
  );
};

export default Sidebar;
