import React from 'react';
import { Table, Space, Image, Typography } from 'antd';
import MtnLogo from '../assets/mtn-circle.png';
import EquityLogo from '../assets/equity-bank-logo.png';
import BKLogo from '../assets/bk-logo.png';
import KCBBank from '../assets/kcb-bank.png';
import Bralirwa from '../assets/bralirwa.png';
import { MdEdit, MdDelete } from 'react-icons/md';
import './table.css';
const StudentTable = () => {
  const { Text, Title } = Typography;
  const columns = [
    {
      title: <Text className="table-title">STUDENTS</Text>,
      dataIndex: 'students',
      key: 'students',
    },
    {
      title: <Text className="table-title">COMPANIES</Text>,
      key: 'companies',
      dataIndex: 'companies',
    },
    {
      title: (
        <Text className="table-title">DATE JOINED</Text>
      ),
      dataIndex: 'dateJoined',
      key: 'date-joined',
    },
    {
      title: (
        <Text className="table-title">SERVICE FEE</Text>
      ),
      key: 'serviceFee',
      dataIndex: 'serviceFee',
    },
  ];

  const dataSource = [
    {
      key: 6,
      students: (
        <Space direction="horizontal">
          <Text>1</Text>
          <Space direction="vertical" className="student">
            <Title className="student-name" level={3}>
              Kamikazi Lilian
            </Title>
            <Text className="student-email">
              {' '}
              KamikaziLili@gmail.com
            </Text>
          </Space>
        </Space>
      ),
      companies: (
        <Space direction="horizontal">
          <Image
            className="table-pic-img"
            width={20}
            height={20}
            src={MtnLogo}
            alt="mtn-logo"
          />
          <Text className="company">MTN Rwanda</Text>
        </Space>
      ),
      dateJoined: (
        <Text className="date">1st,JUNE,2021</Text>
      ),
      serviceFee: <Text className="date-joined">PAID</Text>,
    },
    {
      key: 2,
      students: (
        <Space direction="horizontal">
          <Text>2</Text>
          <Space direction="vertical" className="student">
            <Title className="student-name" level={3}>
              Kamikazi Lilian
            </Title>
            <Text className="student-email">
              KamikaziLili@gmail.com
            </Text>
          </Space>
        </Space>
      ),
      companies: (
        <Space direction="horizontal">
          <Image
            className="table-pic-img"
            width={20}
            height={20}
            src={BKLogo}
            alt="bk-bank-logo"
          />
          <Text className="company">Bank of Kigali</Text>
        </Space>
      ),
      dateJoined: (
        <Text className="date">1st,JUNE,2021</Text>
      ),
      serviceFee: <Text className="date-joined">PAID</Text>,
    },
    {
      key: 3,
      students: (
        <Space direction="horizontal">
          <Text>3</Text>
          <Space direction="vertical" className="student">
            <Title className="student-name" level={3}>
              Kamikazi Lilian
            </Title>
            <Text className="student-email">
              KamikaziLili@gmail.com
            </Text>
          </Space>
        </Space>
      ),
      companies: (
        <Space direction="horizontal">
          <Image
            className="table-pic-img"
            width={25}
            height={25}
            src={Bralirwa}
            alt="bralirwa-logo"
          />
          <Text className="company">Bralirwa</Text>
        </Space>
      ),
      dateJoined: (
        <Text className="date">1st,JUNE,2021</Text>
      ),
      serviceFee: <Text className="date-joined">PAID</Text>,
    },
    {
      key: 4,
      students: (
        <Space direction="horizontal">
          <Text>4</Text>
          <Space direction="vertical" className="student">
            <Title className="student-name" level={3}>
              Kamikazi Lilian
            </Title>
            <Text className="student-email">
              KamikaziLili@gmail.com
            </Text>
          </Space>
        </Space>
      ),
      companies: (
        <Space direction="horizontal">
          <Image
            className="table-pic-img"
            width={20}
            height={20}
            src={KCBBank}
            alt="kcb-bank-logo"
          />
          <Text className="company">KCB Bank</Text>
        </Space>
      ),
      dateJoined: (
        <Text className="date">1st, JUNE, 2021</Text>
      ),
      serviceFee: <Text className="date-joined">PAID</Text>,
    },
    {
      key: 5,
      students: (
        <Space direction="horizontal">
          <Text>5</Text>
          <Space direction="vertical" className="student">
            <Title className="student-name" level={3}>
              Kamikazi Lilian
            </Title>
            <Text className="student-email">
              KamikaziLili@gmail.com
            </Text>
          </Space>
        </Space>
      ),
      companies: (
        <Space direction="horizontal">
          <Image
            className="table-pic-img"
            width={20}
            height={20}
            src={EquityLogo}
            alt="equity-bank-logo"
          />
          <Text className="company">Equity Rwanda</Text>
        </Space>
      ),
      dateJoined: (
        <Text className="date">1st,JUNE,2021</Text>
      ),
      serviceFee: <Text className="date-joined">PAID</Text>,
      editDelete: (
        <div className="rate-button-table">
          <MdEdit />
          <MdDelete />
        </div>
      ),
    },
  ];

  return (
    <>
      <Table
        className="students-table"
        columns={columns}
        dataSource={dataSource}
      />
    </>
  );
};

export default StudentTable;
