import React from 'react';
import { Card, Row, Typography, Space, Image } from 'antd';
import './reportCard.scss';
// import './staffCompanyCard.scss';

const { Text } = Typography;

const ReportTable = ({
  picture,
  companyName,
  name,
  date,
  paid,
  companyEmail,
  id,
}) => {
  return (
    <Card className="cards" size="medium">
      <Row
        className="card-row"
        justify="space-between"
        style={{ width: '100% !important' }}
      >
        <Space
          direction="horizontal"
          className="card-col"
          span=""
        >
          <Text>{id}</Text>
          <Space direction="vertical">
            <Text>{name}</Text>
            <Text>{companyEmail}</Text>
          </Space>
        </Space>
        <Space direction="horizontal">
          <Image
            src={picture}
            width={30}
            heught={30}
            alt="company-picture"
          />
          <Text>{companyName}</Text>
        </Space>
        <Text>{date}</Text>
        <Text>{paid}</Text>
      </Row>
    </Card>
  );
};
export default ReportTable;
