import { Col, Row, Typography } from 'antd';
import './reportHeader.scss';
const { Title } = Typography;

const ReportHeader = () => {
  return (
    <div className="report-header">
      <Col span={12} className="employees-title">
        <Title level={4}>EMPLOYEES</Title>
      </Col>
      <Col span={12}>
        <Title level={4}>COMPANIES</Title>
      </Col>
      <Col span={12}>
        <Title level={4}>DATE JOINED</Title>
      </Col>
      <Col span={6}>
        <Title level={4}>SERVICE FEES</Title>
      </Col>
    </div>
  );
};
export default ReportHeader;
