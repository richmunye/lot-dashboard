import React, { useState } from 'react';
import { Card, Row, Col, Typography, Rate } from 'antd';
import { MdEdit, MdDelete } from 'react-icons/md';
import './card.scss';
const { Text } = Typography;

const Cards = ({ picture, name, company }) => {
  return (
    <Card className="cards" size="medium">
      <Row className="card-row" justify="space-between">
        <Col className="card-col" span="">
          <img src={picture} alt="employee" />
          <Text>{name}</Text>
        </Col>
        <div className="company-name">
          <Text>{company}</Text>
          <div className="rate-button-table">
            <MdEdit />
            <MdDelete />
          </div>
        </div>
      </Row>
    </Card>
  );
};
export default Cards;
