import React from 'react';
import {
  Card,
  Row,
  Col,
  Typography,
  Rate,
  Image,
  Button,
} from 'antd';
import { MdEdit, MdDelete } from 'react-icons/md';
import './card.scss';
import './staffCompanyCard.scss';

const { Text } = Typography;

const StaffCard = ({
  picture,
  company,
  ratings,
  openModal,
}) => {
  return (
    <Card className="cards" size="medium">
      <Row className="card-row" justify="space-between">
        <Col className="card-col" span="">
          <Image
            src={picture}
            alt="employee"
            width={30}
            height={30}
          />
          <Text>{company}</Text>
        </Col>
        <div className="company-name">
          <Rate allowHalf value={ratings} />
          <Button className="rate-btn" onClick={openModal}>
            Rate
          </Button>
          <div className="rate-button-table">
            <MdEdit />
            <MdDelete />
          </div>
        </div>
      </Row>
    </Card>
  );
};
export default StaffCard;
