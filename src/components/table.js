import React, { useState, useEffect } from 'react';
import { Modal } from 'antd';
import RateModal from './rateModal';
import ReactStars from 'react-rating-stars-component';
import { MdEdit, MdDelete } from 'react-icons/md';
import { Space, Typography, Table, Image } from 'antd';

import './table.css';

const { Text } = Typography;
const StudentTableComponent = () => {
  const [ratings, setRatings] = useState('2');

  const [rateModelVisible, setRateModelVisible] =
    useState(false);
  const OpenModel = () => {
    setRateModelVisible(true);
  };
  const handleOk = () => {
    setRateModelVisible(false);
  };

  const columns = [
    {
      dataIndex: 'companies',
      key: 'companies',
    },
    {
      key: 'ratings',
      dataIndex: 'ratings',
    },
    {
      dataIndex: 'rate',
      key: 'rate',
    },
    {
      key: 'editDelete',
      dataIndex: 'editDelete',
    },
  ];

  const dataSource = [
    {
      key: 6,
      companies: (
        <Space direction="horizontal">
          <Image
            className="table-pic-img"
            width={30}
            height={30}
            // src={MtnLogo}
            alt="mtn-logo"
          />
          <Text className="company">MTN Rwanda</Text>
        </Space>
      ),
      ratings: (
        <ReactStars
          count={5}
          onChange={ratings}
          value={ratings}
          isHalf={true}
          onSubmit={ratings}
          emptyIcon={<i className="far fa-star"></i>}
          halfIcon={<i className="fa fa-star-half-alt"></i>}
          fullIcon={<i className="fa fa-star"></i>}
          size={24}
          activeColor="#ffd700"
        />
      ),
      rate: (
        <button onClick={OpenModel} className="date-joined">
          Rate
        </button>
      ),
      editDelete: (
        <div className="rate-button-table">
          <MdEdit />
          <MdDelete />
        </div>
      ),
    },
    {
      key: 2,
      companies: (
        <Space direction="horizontal">
          <Image
            className="table-pic-img"
            width={30}
            height={30}
            // src={BKLogo}
            alt="bk-bank-logo"
          />
          <Text className="company">Bank of Kigali</Text>
        </Space>
      ),
      ratings: (
        <ReactStars
          count={5}
          onChange={ratings}
          value={ratings}
          isHalf={true}
          emptyIcon={<i className="far fa-star"></i>}
          halfIcon={<i className="fa fa-star-half-alt"></i>}
          fullIcon={<i className="fa fa-star"></i>}
          size={24}
          activeColor="#ffd700"
        />
      ),
      rate: <button className="date-joined">Rate</button>,
      editDelete: (
        <div className="rate-button-table">
          <MdEdit />
          <MdDelete />
        </div>
      ),
    },
    {
      key: 3,
      companies: (
        <Space direction="horizontal">
          <Image
            className="table-pic-img"
            width={30}
            height={30}
            // src={Bralirwa}
            alt="bralirwa-logo"
          />
          <Text className="company">Bralirwa</Text>
        </Space>
      ),
      ratings: (
        <ReactStars
          count={5}
          value={ratings}
          isHalf={true}
          emptyIcon={<i className="far fa-star"></i>}
          halfIcon={<i className="fa fa-star-half-alt"></i>}
          fullIcon={<i className="fa fa-star"></i>}
          size={24}
          activeColor="#ffd700"
        />
      ),
      rate: <button className="date-joined">Rate</button>,
      editDelete: (
        <div className="rate-button-table">
          <MdEdit />
          <MdDelete />
        </div>
      ),
    },
    {
      key: 4,
      companies: (
        <Space direction="horizontal">
          <Image
            className="table-pic-img"
            width={30}
            height={30}
            // src={KCBBank}
            alt="kcb-bank-logo"
          />
          <Text className="company">KCB Bank</Text>
        </Space>
      ),
      ratings: (
        <ReactStars
          count={5}
          value={ratings}
          isHalf={true}
          emptyIcon={<i className="far fa-star"></i>}
          halfIcon={<i className="fa fa-star-half-alt"></i>}
          fullIcon={<i className="fa fa-star"></i>}
          size={24}
          activeColor="#ffd700"
        />
      ),
      rate: <button className="date-joined">Rate</button>,
      editDelete: (
        <div className="rate-button-table">
          <MdEdit />
          <MdDelete />
        </div>
      ),
    },
    {
      key: 5,
      companies: (
        <Space direction="horizontal">
          <Image
            className="table-pic-img"
            width={30}
            height={30}
            // src={EquityLogo}
            alt="equity-bank-logo"
          />
          <Text className="company">Equity Rwanda</Text>
        </Space>
      ),
      ratings: (
        <ReactStars
          count={5}
          // onChange={ratingChanged}
          value={ratings}
          isHalf={true}
          emptyIcon={<i className="far fa-star"></i>}
          halfIcon={<i className="fa fa-star-half-alt"></i>}
          fullIcon={<i className="fa fa-star"></i>}
          size={24}
          activeColor="#ffd700"
        />
      ),
      rate: (
        <button onClick={OpenModel} className="date-joined">
          Rate
        </button>
      ),
      editDelete: (
        <div className="rate-button-table">
          <MdEdit />
          <MdDelete />
        </div>
      ),
    },
  ];

  const data = dataSource.map((date) => {
    return date;
  });

  return (
    <>
      <Modal
        style={{ top: 10 }}
        visible={rateModelVisible}
        cancelButtonProps={false}
        centered
        onOk={handleOk}
        onCancel={() => setRateModelVisible(false)}
      >
        <RateModal handleOk={handleOk} />
      </Modal>
      <Table
        className="students-table"
        columns={columns}
        dataSource={dataSource}
      />
    </>
  );
};

export default StudentTableComponent;
