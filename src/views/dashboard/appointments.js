import React, { useState } from 'react';
import { Layout, Menu, Col, Row, Input } from 'antd';
import './dashboard.css';
import AppointmentSvg from '../../assets/Icon ionic-ios-calendar.svg';
import StaffIcon from '../../assets/Icon awesome-user-alt.svg';
import ReportsIcon from '../../assets/article_black_24dp.svg';
import SettingsIcon from '../../assets/Icon material-settings.svg';
import ProfilePic from '../../assets/profile-pic.png';
import HideIconOn from '../../assets/hide.png';
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';
import Sidebar from '../../components/sidebar/sidebar';

const { Header, Content, Sider } = Layout;

const Appointments = () => {
  const [collapsed, setcollapsed] = useState(false);

  const [darkmode, setdarkmode] = useState('light');

  const themechange = () => {
    if (darkmode === 'dark') setdarkmode('light');
    if (darkmode === 'light') setdarkmode('dark');
  };

  const onCollapse = (collapsed) => {
    setcollapsed(onCollapse);
  };

  const collapsedChange = () => {
    setcollapsed(!collapsed);
  };

  return (
    <Layout
      style={{ minHeight: '100vh' }}
      className="euclid_circular_b_font"
    >
      {/* <SideBar/> */}
      <Sidebar />
      <Layout
        className={`site-layout ${
          darkmode === 'dark' ? 'dark_mode_background' : ''
        }`}
      >
        <Header
          theme={darkmode}
          className={`site-layout-background dashboard-header ${
            darkmode === 'dark'
              ? 'dark_mode_background'
              : ''
          }`}
          style={{ padding: 0 }}
        >
          <Row>
            <Col span={6}>
              {' '}
              <h1
                className={`dark_blue_color ${
                  darkmode === 'dark'
                    ? 'light_green_color'
                    : ''
                }`}
              >
                Appointments
              </h1>{' '}
            </Col>
            <Col>
              <Input
                className="search-input dark"
                size="large"
                placeholder="Search for patient"
                prefix={
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 24 24"
                    width="16"
                    height="16"
                  >
                    <path fill="none" d="M0 0h24v24H0z" />
                    <path
                      d="M11 2c4.968 0 9 4.032 9 9s-4.032 9-9 9-9-4.032-9-9 4.032-9 9-9zm0 16c3.867 0 7-3.133 7-7 0-3.868-3.133-7-7-7-3.868 0-7 3.132-7 7 0 3.867 3.132 7 7 7zm8.485.071l2.829 2.828-1.415 1.415-2.828-2.829 1.414-1.414z"
                      fill="rgba(203,203,203,1)"
                    />
                  </svg>
                }
              />
            </Col>
            <Col className="action-menu">
              <Row>
                <Col span={8}>
                  {darkmode === 'dark' ? (
                    <svg
                      onClick={themechange}
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 24 24"
                      width="24"
                      height="24"
                    >
                      <path fill="none" d="M0 0h24v24H0z" />
                      <path
                        d="M11.38 2.019a7.5 7.5 0 1 0 10.6 10.6C21.662 17.854 17.316 22 12.001 22 6.477 22 2 17.523 2 12c0-5.315 4.146-9.661 9.38-9.981z"
                        fill="rgba(255,255,255,1)"
                      />
                    </svg>
                  ) : (
                    <svg
                      onClick={themechange}
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 24 24"
                      width="24"
                      height="24"
                    >
                      <path fill="none" d="M0 0h24v24H0z" />
                      <path d="M11.38 2.019a7.5 7.5 0 1 0 10.6 10.6C21.662 17.854 17.316 22 12.001 22 6.477 22 2 17.523 2 12c0-5.315 4.146-9.661 9.38-9.981z" />
                    </svg>
                  )}
                </Col>

                <Col>
                  {darkmode === 'dark' ? (
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 24 24"
                      width="24"
                      height="24"
                    >
                      <path fill="none" d="M0 0h24v24H0z" />
                      <path
                        d="M5 22a1 1 0 0 1-1-1V3a1 1 0 0 1 1-1h14a1 1 0 0 1 1 1v3h-2V4H6v16h12v-2h2v3a1 1 0 0 1-1 1H5zm13-6v-3h-7v-2h7V8l5 4-5 4z"
                        fill="rgba(255,255,255,1)"
                      />
                    </svg>
                  ) : (
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 24 24"
                      width="24"
                      height="24"
                    >
                      <path fill="none" d="M0 0h24v24H0z" />
                      <path d="M5 22a1 1 0 0 1-1-1V3a1 1 0 0 1 1-1h14a1 1 0 0 1 1 1v3h-2V4H6v16h12v-2h2v3a1 1 0 0 1-1 1H5zm13-6v-3h-7v-2h7V8l5 4-5 4z" />
                    </svg>
                  )}
                </Col>
              </Row>
            </Col>
          </Row>
        </Header>
        <Content>
          <Row>
            <Col
              className={`dash-appointment-content-right ${
                darkmode === 'dark'
                  ? 'dark_mode_background'
                  : ''
              }`}
              span={16}
              // style={{ height: '545px', width: '100%' }}
            >
              <h1
                className={`appointment-header ${
                  darkmode === 'dark'
                    ? 'pure_white_color'
                    : ''
                }`}
              >
                Calendar
              </h1>
              <Calendar
                style={{
                  backgroundColor: 'white',
                  width: '600px',
                  border: 'none',
                }}
              />
              <h1
                className={`appointment-header events ${
                  darkmode === 'dark'
                    ? 'pure_white_color'
                    : ''
                }`}
              >
                June main Events
              </h1>
              <div className="calendar-meeting">
                <Row
                  className={`container ${
                    darkmode === 'dark'
                      ? 'pure_white_background'
                      : ''
                  }`}
                  style={{
                    boxShadow: '2px 4px 4px #3565d329',
                  }}
                >
                  <Col span={4}>
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 24 24"
                      width="24"
                      height="24"
                    >
                      <path fill="none" d="M0 0h24v24H0z" />
                      <path
                        d="M2 22a8 8 0 1 1 16 0H2zm8-9c-3.315 0-6-2.685-6-6s2.685-6 6-6 6 2.685 6 6-2.685 6-6 6zm7.363 2.233A7.505 7.505 0 0 1 22.983 22H20c0-2.61-1-4.986-2.637-6.767zm-2.023-2.276A7.98 7.98 0 0 0 18 7a7.964 7.964 0 0 0-1.015-3.903A5 5 0 0 1 21 8a4.999 4.999 0 0 1-5.66 4.957z"
                        fill="rgba(27,39,175,1)"
                      />
                    </svg>
                  </Col>
                  <Col span={18}>
                    <p className="appointment-type ">
                      General meeting
                    </p>
                    <p className="appointment-time">
                      10 - 11 pm
                    </p>
                  </Col>
                  <Col span={2}>
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 24 24"
                      width="24"
                      height="24"
                    >
                      <path fill="none" d="M0 0h24v24H0z" />
                      <path
                        d="M12 3c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm0 14c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm0-7c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2z"
                        fill="rgba(203,203,203,1)"
                      />
                    </svg>
                  </Col>
                </Row>
                <Row
                  className={`container ${
                    darkmode === 'dark'
                      ? 'pure_white_background'
                      : ''
                  }`}
                  style={{
                    boxShadow: '2px 4px 4px #3565d329',
                  }}
                >
                  <Col span={4}>
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 24 24"
                      width="24"
                      height="24"
                    >
                      <path fill="none" d="M0 0h24v24H0z" />
                      <path
                        d="M14 10h-4v4h4v-4zm2 0v4h3v-4h-3zm-2 9v-3h-4v3h4zm2 0h3v-3h-3v3zM14 5h-4v3h4V5zm2 0v3h3V5h-3zm-8 5H5v4h3v-4zm0 9v-3H5v3h3zM8 5H5v3h3V5zM4 3h16a1 1 0 0 1 1 1v16a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V4a1 1 0 0 1 1-1z"
                        fill="rgba(26,179,116,1)"
                      />
                    </svg>
                  </Col>
                  <Col span={18}>
                    <p className="appointment-type">
                      Uni Marketing
                    </p>
                    <p className="appointment-time">
                      10 - 11 pm
                    </p>
                  </Col>
                  <Col span={2}>
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 24 24"
                      width="24"
                      height="24"
                    >
                      <path fill="none" d="M0 0h24v24H0z" />
                      <path
                        d="M12 3c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm0 14c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm0-7c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2z"
                        fill="rgba(203,203,203,1)"
                      />
                    </svg>
                  </Col>
                </Row>
                <Row
                  className={`container ${
                    darkmode === 'dark'
                      ? 'pure_white_background'
                      : ''
                  }`}
                  style={{
                    boxShadow: '2px 4px 4px #3565d329',
                  }}
                >
                  <Col span={4}>
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 24 24"
                      width="24"
                      height="24"
                    >
                      <path fill="none" d="M0 0h24v24H0z" />
                      <path
                        d="M2 22a8 8 0 1 1 16 0H2zm8-9c-3.315 0-6-2.685-6-6s2.685-6 6-6 6 2.685 6 6-2.685 6-6 6zm7.363 2.233A7.505 7.505 0 0 1 22.983 22H20c0-2.61-1-4.986-2.637-6.767zm-2.023-2.276A7.98 7.98 0 0 0 18 7a7.964 7.964 0 0 0-1.015-3.903A5 5 0 0 1 21 8a4.999 4.999 0 0 1-5.66 4.957z"
                        fill="rgba(27,39,175,1)"
                      />
                    </svg>
                  </Col>
                  <Col span={18}>
                    <p className="appointment-type">
                      General meeting
                    </p>
                    <p className="appointment-time">
                      10 - 11 pm
                    </p>
                  </Col>
                  <Col span={2}>
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 24 24"
                      width="24"
                      height="24"
                    >
                      <path fill="none" d="M0 0h24v24H0z" />
                      <path
                        d="M12 3c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm0 14c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm0-7c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2z"
                        fill="rgba(203,203,203,1)"
                      />
                    </svg>
                  </Col>
                </Row>
              </div>
            </Col>
            <Col
              span={6}
              className="dash-center-content-left"
              style={{ height: '500px' }}
            >
              <div className="contents">
                <div>
                  <h3
                    className={`${
                      darkmode === 'dark'
                        ? 'pure_white_color'
                        : 'date-status'
                    }`}
                  >
                    Today, 10 June
                  </h3>
                </div>
                <Row className="container">
                  <Col span={4}>
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 24 24"
                      width="24"
                      height="24"
                    >
                      <path fill="none" d="M0 0h24v24H0z" />
                      <path
                        d="M2 22a8 8 0 1 1 16 0H2zm8-9c-3.315 0-6-2.685-6-6s2.685-6 6-6 6 2.685 6 6-2.685 6-6 6zm7.363 2.233A7.505 7.505 0 0 1 22.983 22H20c0-2.61-1-4.986-2.637-6.767zm-2.023-2.276A7.98 7.98 0 0 0 18 7a7.964 7.964 0 0 0-1.015-3.903A5 5 0 0 1 21 8a4.999 4.999 0 0 1-5.66 4.957z"
                        fill="rgba(27,39,175,1)"
                      />
                    </svg>
                  </Col>
                  <Col span={18}>
                    <p className="appointment-type">
                      General meeting
                    </p>
                    <p className="appointment-time">
                      10 - 11 pm
                    </p>
                  </Col>
                  <Col span={2}>
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 24 24"
                      width="24"
                      height="24"
                    >
                      <path fill="none" d="M0 0h24v24H0z" />
                      <path
                        d="M12 3c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm0 14c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm0-7c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2z"
                        fill="rgba(203,203,203,1)"
                      />
                    </svg>
                  </Col>
                </Row>

                <Row className="container">
                  <Col span={4}>
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 24 24"
                      width="24"
                      height="24"
                    >
                      <path fill="none" d="M0 0h24v24H0z" />
                      <path
                        d="M14 10h-4v4h4v-4zm2 0v4h3v-4h-3zm-2 9v-3h-4v3h4zm2 0h3v-3h-3v3zM14 5h-4v3h4V5zm2 0v3h3V5h-3zm-8 5H5v4h3v-4zm0 9v-3H5v3h3zM8 5H5v3h3V5zM4 3h16a1 1 0 0 1 1 1v16a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V4a1 1 0 0 1 1-1z"
                        fill="rgba(26,179,116,1)"
                      />
                    </svg>
                  </Col>
                  <Col span={18}>
                    <p className="appointment-type">
                      Uni Marketing
                    </p>
                    <p className="appointment-time">
                      10 - 11 pm
                    </p>
                  </Col>
                  <Col span={2}>
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 24 24"
                      width="24"
                      height="24"
                    >
                      <path fill="none" d="M0 0h24v24H0z" />
                      <path
                        d="M12 3c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm0 14c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm0-7c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2z"
                        fill="rgba(203,203,203,1)"
                      />
                    </svg>
                  </Col>
                </Row>

                <Row className="container">
                  <Col span={4}>
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 24 24"
                      width="24"
                      height="24"
                    >
                      <path fill="none" d="M0 0h24v24H0z" />
                      <path
                        d="M2 22a8 8 0 1 1 16 0H2zm8-9c-3.315 0-6-2.685-6-6s2.685-6 6-6 6 2.685 6 6-2.685 6-6 6zm7.363 2.233A7.505 7.505 0 0 1 22.983 22H20c0-2.61-1-4.986-2.637-6.767zm-2.023-2.276A7.98 7.98 0 0 0 18 7a7.964 7.964 0 0 0-1.015-3.903A5 5 0 0 1 21 8a4.999 4.999 0 0 1-5.66 4.957z"
                        fill="rgba(27,39,175,1)"
                      />
                    </svg>
                  </Col>
                  <Col span={18}>
                    <p className="appointment-type">
                      General meeting
                    </p>
                    <p className="appointment-time">
                      10 - 11 pm
                    </p>
                  </Col>
                  <Col span={2}>
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 24 24"
                      width="24"
                      height="24"
                    >
                      <path fill="none" d="M0 0h24v24H0z" />
                      <path
                        d="M12 3c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm0 14c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm0-7c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2z"
                        fill="rgba(203,203,203,1)"
                      />
                    </svg>
                  </Col>
                </Row>
              </div>
            </Col>
          </Row>
        </Content>
        {/* <Footer style={{ textAlign: 'center' }}>Task force 3 ©2020 Created by Egide Ntwari</Footer> */}
      </Layout>
    </Layout>
  );
};
export default Appointments;
