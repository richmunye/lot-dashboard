import React, { useState } from 'react';
import { Layout, Menu, Col, Row, Input } from 'antd';
import { Bar } from 'nivo';
import './dashboard.css';
import AppointmentSvg from '../../assets/Icon ionic-ios-calendar.svg';
import StaffIcon from '../../assets/Icon awesome-user-alt.svg';
import ReportsIcon from '../../assets/article_black_24dp.svg';
import SettingsIcon from '../../assets/Icon material-settings.svg';
import ProfilePic from '../../assets/profile-pic.png';
import HideIconOn from '../../assets/hide.png';
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
} from 'recharts';
import Sidebar from '../../components/sidebar/sidebar';

const { Header, Content, Sider } = Layout;

const WelcomeDashboard = () => {
  const data = [
    {
      name: 'Text',
      uv: 3490,
      pv: 0,
      amt: 2100,
    },
    {
      name: 'Text',
      uv: 4000,
      pv: 12,
      amt: 2400,
    },
    {
      name: 'Text',
      uv: 3000,
      pv: 23,
      amt: 2210,
    },
    {
      name: 'Text',
      uv: 2000,
      pv: 14,
      amt: 2290,
    },
    {
      name: 'Text',
      uv: 2780,
      pv: 56,
      amt: 2000,
    },
    {
      name: 'Page E',
      uv: 1890,
      pv: 4800,
      amt: 2181,
    },
    {
      name: 'Text',
      uv: 2390,
      pv: 71,
      amt: 2500,
    },
    {
      name: 'Text',
      uv: 3490,
      pv: 34,
      amt: 2100,
    },
    {
      name: 'Text',
      uv: 3490,
      pv: 14,
      amt: 2100,
    },
    {
      name: 'Text',
      uv: 3490,
      pv: 14,
      amt: 2100,
    },
    {
      name: 'Text',
      uv: 3490,
      pv: 23,
      amt: 2100,
    },
    {
      name: 'Text',
      uv: 390,
      pv: 0,
      amt: 2100,
    },
  ];

  const newdata = [
    {
      id: 'A',
      students: 13,
    },
    {
      id: 'B',
      students: 5,
    },
    {
      id: 'C',
      students: 40,
    },
    {
      id: 'D',
      students: 23,
    },
    {
      id: 'E',
      students: 25,
    },
    {
      id: 'F',
      students: 25,
    },
    {
      id: 'G',
      students: 12,
    },
  ];

  const [collapsed, setcollapsed] = useState(false);

  const [darkmode, setdarkmode] = useState('light');

  const themechange = () => {
    if (darkmode === 'dark') setdarkmode('light');
    if (darkmode === 'light') setdarkmode('dark');
  };

  const onCollapse = (collapsed) => {
    setcollapsed(onCollapse);
  };

  const collapsedChange = () => {
    setcollapsed(!collapsed);
  };

  return (
    <Layout
      style={{ minHeight: '100vh' }}
      className="euclid_circular_b_font"
    >
      <Sidebar />
      <Layout
        className={`site-layout ${
          darkmode === 'dark' ? 'dark_mode_background' : ''
        }`}
      >
        <Header
          theme={darkmode}
          className={`site-layout-background dashboard-header ${
            darkmode === 'dark'
              ? 'dark_mode_background'
              : ''
          }`}
          style={{ padding: 0 }}
        >
          <Row>
            <Col span={6}>
              {' '}
              <h1
                className={`dark_blue_color ${
                  darkmode === 'dark'
                    ? 'light_green_color'
                    : ''
                }`}
              >
                Overview
              </h1>{' '}
            </Col>
            <Col>
              <Input
                className="search-input dark"
                size="large"
                placeholder="Search for patient"
                prefix={
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 24 24"
                    width="16"
                    height="16"
                  >
                    <path fill="none" d="M0 0h24v24H0z" />
                    <path
                      d="M11 2c4.968 0 9 4.032 9 9s-4.032 9-9 9-9-4.032-9-9 4.032-9 9-9zm0 16c3.867 0 7-3.133 7-7 0-3.868-3.133-7-7-7-3.868 0-7 3.132-7 7 0 3.867 3.132 7 7 7zm8.485.071l2.829 2.828-1.415 1.415-2.828-2.829 1.414-1.414z"
                      fill="rgba(203,203,203,1)"
                    />
                  </svg>
                }
              />
            </Col>
            <Col className="action-menu">
              <Row>
                <Col span={8}>
                  {darkmode === 'dark' ? (
                    <svg
                      onClick={themechange}
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 24 24"
                      width="24"
                      height="24"
                    >
                      <path fill="none" d="M0 0h24v24H0z" />
                      <path
                        d="M11.38 2.019a7.5 7.5 0 1 0 10.6 10.6C21.662 17.854 17.316 22 12.001 22 6.477 22 2 17.523 2 12c0-5.315 4.146-9.661 9.38-9.981z"
                        fill="rgba(255,255,255,1)"
                      />
                    </svg>
                  ) : (
                    <svg
                      onClick={themechange}
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 24 24"
                      width="24"
                      height="24"
                    >
                      <path fill="none" d="M0 0h24v24H0z" />
                      <path d="M11.38 2.019a7.5 7.5 0 1 0 10.6 10.6C21.662 17.854 17.316 22 12.001 22 6.477 22 2 17.523 2 12c0-5.315 4.146-9.661 9.38-9.981z" />
                    </svg>
                  )}
                </Col>

                <Col>
                  {darkmode === 'dark' ? (
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 24 24"
                      width="24"
                      height="24"
                    >
                      <path fill="none" d="M0 0h24v24H0z" />
                      <path
                        d="M5 22a1 1 0 0 1-1-1V3a1 1 0 0 1 1-1h14a1 1 0 0 1 1 1v3h-2V4H6v16h12v-2h2v3a1 1 0 0 1-1 1H5zm13-6v-3h-7v-2h7V8l5 4-5 4z"
                        fill="rgba(255,255,255,1)"
                      />
                    </svg>
                  ) : (
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 24 24"
                      width="24"
                      height="24"
                    >
                      <path fill="none" d="M0 0h24v24H0z" />
                      <path d="M5 22a1 1 0 0 1-1-1V3a1 1 0 0 1 1-1h14a1 1 0 0 1 1 1v3h-2V4H6v16h12v-2h2v3a1 1 0 0 1-1 1H5zm13-6v-3h-7v-2h7V8l5 4-5 4z" />
                    </svg>
                  )}
                </Col>
              </Row>
            </Col>
          </Row>
        </Header>
        <Content>
          <Row>
            <Col
              className={`dash-center-content-right ${
                darkmode === 'dark'
                  ? 'dark_mode_background'
                  : ''
              }`}
              span={18}
            >
              <h1
                className={`main-header ${
                  darkmode === 'dark'
                    ? 'pure_white_color'
                    : ''
                }`}
              >
                Welcome to <strong>Lot,</strong>
              </h1>

              <div className="poppins_font chart-container">
                <h3
                  className={`${
                    darkmode === 'dark'
                      ? 'pure_white_color'
                      : ''
                  }`}
                >
                  Success Map
                </h3>

                <ResponsiveContainer
                  width="100%"
                  height="90%"
                >
                  <LineChart
                    width={500}
                    height={300}
                    data={data}
                    margin={{
                      top: 5,
                      right: 30,
                      left: 20,
                      bottom: 5,
                    }}
                  >
                    <CartesianGrid strokeDasharray="3 3" />
                    <XAxis dataKey="name" />
                    <YAxis />
                    <Tooltip />
                    <Legend>Success Map </Legend>
                    <Line
                      type="monotone"
                      dataKey="pv"
                      stroke="#8884d8"
                      activeDot={{ r: 8 }}
                    />
                    <Line
                      type="monotone"
                      dataKey="uv"
                      stroke="#82ca9d"
                    />
                  </LineChart>
                </ResponsiveContainer>
              </div>

              <Row>
                <Col
                  className="summary-container dark_blue_backgroun"
                  span={8}
                >
                  <h4 className="pure_white_color">
                    EMPLOYERS
                  </h4>
                  <h2 className="light_green_color">36</h2>
                  <p className="pure_white_color">
                    Employers finds their employees through
                    us
                  </p>
                </Col>
                <Col
                  className="summary-container dark_blue_backgroun this"
                  span={7}
                >
                  <h4 className="pure_white_color">JOBS</h4>
                  <h2 className="light_green_color">
                    36,390
                  </h2>
                  <p className="pure_white_color">
                    Employers finds their employees through
                    us
                  </p>
                </Col>
                <Col
                  className="summary-container dark_blue_backgroun this"
                  span={7}
                >
                  <h4 className="pure_white_color">
                    ONGOING PROJECTS
                  </h4>
                  <h2 className="light_green_color">36</h2>
                  <p className="pure_white_color">
                    Employees are training to find jobs
                  </p>
                </Col>
              </Row>

              <div className="poppins_font chart-container chart-container-2">
                <h3
                  className={`${
                    darkmode === 'dark'
                      ? 'pure_white_color'
                      : ''
                  }`}
                >
                  Student Rates
                </h3>

                <Bar
                  data={newdata}
                  keys={['students']}
                  colors={[
                    darkmode === 'dark'
                      ? '#00FFEB'
                      : '#0028AF',
                  ]}
                  width={500}
                  height={320}
                  padding={0.5}
                  margin={{
                    top: 30,
                    right: 0,
                    bottom: 50,
                    left: 50,
                  }}
                  enableLabel={false}
                  enableGridY={true}
                />
              </div>
            </Col>
            <Col
              span={6}
              className="dash-center-content-left"
            >
              <div className="contents">
                <div>
                  <h3
                    className={`${
                      darkmode === 'dark'
                        ? 'pure_white_color'
                        : 'date-status'
                    }`}
                  >
                    Today, 10 June
                  </h3>
                </div>
                <Row className="container">
                  <Col span={4}>
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 24 24"
                      width="24"
                      height="24"
                    >
                      <path fill="none" d="M0 0h24v24H0z" />
                      <path
                        d="M2 22a8 8 0 1 1 16 0H2zm8-9c-3.315 0-6-2.685-6-6s2.685-6 6-6 6 2.685 6 6-2.685 6-6 6zm7.363 2.233A7.505 7.505 0 0 1 22.983 22H20c0-2.61-1-4.986-2.637-6.767zm-2.023-2.276A7.98 7.98 0 0 0 18 7a7.964 7.964 0 0 0-1.015-3.903A5 5 0 0 1 21 8a4.999 4.999 0 0 1-5.66 4.957z"
                        fill="rgba(27,39,175,1)"
                      />
                    </svg>
                  </Col>
                  <Col span={18}>
                    <p className="action-type">
                      General meeting
                    </p>
                    <p className="action-time">
                      10 - 11 pm
                    </p>
                  </Col>
                  <Col span={2}>
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 24 24"
                      width="24"
                      height="24"
                    >
                      <path fill="none" d="M0 0h24v24H0z" />
                      <path
                        d="M12 3c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm0 14c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm0-7c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2z"
                        fill="rgba(203,203,203,1)"
                      />
                    </svg>
                  </Col>
                </Row>

                <Row className="container">
                  <Col span={4}>
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 24 24"
                      width="24"
                      height="24"
                    >
                      <path fill="none" d="M0 0h24v24H0z" />
                      <path
                        d="M14 10h-4v4h4v-4zm2 0v4h3v-4h-3zm-2 9v-3h-4v3h4zm2 0h3v-3h-3v3zM14 5h-4v3h4V5zm2 0v3h3V5h-3zm-8 5H5v4h3v-4zm0 9v-3H5v3h3zM8 5H5v3h3V5zM4 3h16a1 1 0 0 1 1 1v16a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V4a1 1 0 0 1 1-1z"
                        fill="rgba(26,179,116,1)"
                      />
                    </svg>
                  </Col>
                  <Col span={18}>
                    <p className="action-type">
                      Uni Marketing
                    </p>
                    <p className="action-time">
                      10 - 11 pm
                    </p>
                  </Col>
                  <Col span={2}>
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 24 24"
                      width="24"
                      height="24"
                    >
                      <path fill="none" d="M0 0h24v24H0z" />
                      <path
                        d="M12 3c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm0 14c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm0-7c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2z"
                        fill="rgba(203,203,203,1)"
                      />
                    </svg>
                  </Col>
                </Row>

                <Row className="container">
                  <Col span={4}>
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 24 24"
                      width="24"
                      height="24"
                    >
                      <path fill="none" d="M0 0h24v24H0z" />
                      <path
                        d="M2 22a8 8 0 1 1 16 0H2zm8-9c-3.315 0-6-2.685-6-6s2.685-6 6-6 6 2.685 6 6-2.685 6-6 6zm7.363 2.233A7.505 7.505 0 0 1 22.983 22H20c0-2.61-1-4.986-2.637-6.767zm-2.023-2.276A7.98 7.98 0 0 0 18 7a7.964 7.964 0 0 0-1.015-3.903A5 5 0 0 1 21 8a4.999 4.999 0 0 1-5.66 4.957z"
                        fill="rgba(27,39,175,1)"
                      />
                    </svg>
                  </Col>
                  <Col span={18}>
                    <p className="action-type">
                      General meeting
                    </p>
                    <p className="action-time">
                      10 - 11 pm
                    </p>
                  </Col>
                  <Col span={2}>
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 24 24"
                      width="24"
                      height="24"
                    >
                      <path fill="none" d="M0 0h24v24H0z" />
                      <path
                        d="M12 3c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm0 14c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm0-7c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2z"
                        fill="rgba(203,203,203,1)"
                      />
                    </svg>
                  </Col>
                </Row>
              </div>
            </Col>
          </Row>
        </Content>
      </Layout>
    </Layout>
  );
};

export default WelcomeDashboard;
