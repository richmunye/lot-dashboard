import React, { useState } from 'react';
import Dashboard from '../../components/dashboard-component';
import './dashboard.css';
import { connect } from 'react-redux';
import {
  Button,
  Row,
  Col,
  Switch,
  Space,
  Upload,
  Modal,
  Form,
  Input,
  Select,
} from 'antd';
import TableComponent from '../../components/table';
import Employees from '../../components/employeesTable';
import './staffs.css';
const Staffs = (props) => {
  console.log(props.companies);
  const [employers, setEmployers] = useState(true);
  const [modelVisible, setModelVisible] = useState(false);

  const { Option } = Select;
  const handleOk = () => {
    setModelVisible(false);
  };
  return (
    <Dashboard dashboardName="Staffs" active={3}>
      <Modal
        className="add-modal"
        title="Add Company"
        style={{ top: 20 }}
        visible={modelVisible}
        cancelButtonProps={false}
        centered
        onCancel={handleOk}
      >
        <Upload
          name="avatar"
          listType="picture-card"
          className="avatar-uploader"
          showUploadList={false}
          action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 24 24"
            width="72"
            height="72"
          >
            <path fill="none" d="M0 0h24v24H0z" />
            <path
              d="M4 22a8 8 0 1 1 16 0H4zm8-9c-3.315 0-6-2.685-6-6s2.685-6 6-6 6 2.685 6 6-2.685 6-6 6z"
              fill="rgba(208,208,208,1)"
            />
          </svg>
        </Upload>

        <Form
          name="normal_login"
          className="model-form-container"
        >
          <Form.Item
            name="name of company"
            rules={[
              {
                required: true,
                message: 'Please input your Username!',
              },
            ]}
          >
            <Input placeholder="Name of the company" />
          </Form.Item>

          <Form.Item
            name="username"
            rules={[
              {
                required: true,
                message: 'Please input your Username!',
              },
            ]}
          >
            <Select
              defaultValue="lucy"
              className="selector"
            >
              <Option value="lucy" className="options">
                Select a Company
              </Option>
              <Option className="options" value="jack">
                Small Company ( 10 - 100 )
              </Option>
              <Option value="jack">
                Medium Company ( 100 - 1000 )
              </Option>
            </Select>
          </Form.Item>
          <div className="modal-btn">
            <Button onClick={handleOk} type="primary">
              ADD
            </Button>
          </div>
        </Form>
      </Modal>
      <Row
        style={{
          padding: '20px',
          display: 'flex',
          justifyContent: 'space-between',
        }}
      >
        <Col span={6}>
          <Space
            className="switch-content"
            direction="vertical"
          >
            <Switch
              checkedChildren="Employers"
              unCheckedChildren="Employees"
              checked={employers}
              onChange={() => setEmployers(!employers)}
            />
          </Space>
        </Col>
        <Col span={8}>
          <Button
            className="ant-btn"
            style={{
              backgroundColor: '#0028af',
              color: 'white',
            }}
            onClick={() => setModelVisible(true)}
          >
            ADD
          </Button>
        </Col>
      </Row>
      {employers ? (
        <TableComponent employers={false} />
      ) : (
        <Employees />
      )}
    </Dashboard>
  );
};
const mapStateToProps = (state) => {
  return {
    companies: state.companies,
  };
};

export default connect(mapStateToProps)(Staffs);
