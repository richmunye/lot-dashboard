// import React from 'react';
import Dashboard from '../components/dashboard-component';
import {
  Typography,
  Row,
  Col,
  Space,
  DatePicker,
  Layout,
} from 'antd';
// import StudentTable from '../components/students-table';
// import './reports.scss';

// const Reports = () => {
//   return (
//     <Dashboard dashboardName="Reports" active={4}>
//       <Layout className="student-rate">
//         <StudentTable />
//       </Layout>
//     </Dashboard>
//   );
// };

// export default Reports;
import StudentTable from '../components/students-table';
import './reports.scss';
const { Title } = Typography;

const { Text } = Typography;

const Reports = () => {
  let darkmode = window.localStorage.getItem('theme');

  return (
    <Dashboard dashboardName="Reports" active={4}>
      <Row>
        <Col span={14}>
          <Title
            className={`main-header ${
              darkmode === 'dark' ? 'pure_white_color' : ''
            }`}
          >
            Students rate
          </Title>
        </Col>
        <Col span={10}>
          <Space
            direction="horizontal"
            className="choose-date-container"
          >
            <Text className="start">From</Text>
            <DatePicker className="date-classes" />
            <Text className="end">To</Text>{' '}
            <DatePicker className="date-classes" />
          </Space>
        </Col>
      </Row>

      <Layout className="student-rate">
        <StudentTable />
      </Layout>
    </Dashboard>
  );
};

export default Reports;
