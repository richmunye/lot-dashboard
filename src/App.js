import './App.css';

import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom';

import LoginComponent from './views/Login/Login';
import './index.css';
import './styles/project-theme.css';
import './assets/font/stylesheet.css';
import 'bootstrap/dist/css/bootstrap.css';
import SignupComponent from './views/Login/Signup';
import WelcomeDashboard from './views/dashboard/welcome-dashboard';
import Appointments from './views/dashboard/appointments';
import Staffs from './views/dashboard/staffs';
import Reports from './views/reports';

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <LoginComponent />
        </Route>
        <Route
          exact
          path="/signup"
          component={SignupComponent}
        />
        <Route
          exact
          path="/dashboard"
          component={WelcomeDashboard}
        />
        <Route
          exact
          path="/appointments"
          component={Appointments}
        />
        <Route exact path="/staffs" component={Staffs} />
        <Route exact path="/reports" component={Reports} />
      </Switch>
    </Router>
  );
}

export default App;
