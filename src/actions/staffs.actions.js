import axios from 'axios';
import {
  GET_COMPANY,
  GET_COMPANY_LOADING,
  GET_EMPLOYEES_LOADING,
  GET_EMPLOYEES,
} from '../actionTypes/staffs.types';

const baseUrl = process.env.REACT_APP_BASEURL;

const StaffActions = () => (dispatch) => {
  const config = {
    headers: {
      'Content-Type': 'application/json',
    },
  };
  axios
    .get(`${baseUrl}/companies`, config)
    .then((response) => {
      console.log('response', response);
      dispatch({
        type: GET_COMPANY,
        payload: response,
      });
    })
    .catch((error) => console.log('the response ', error));
};
export default StaffActions;
